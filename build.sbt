name := """payment-processor"""
organization := "home"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  javaJpa,
  "org.hibernate" % "hibernate-core" % "5.2.10.Final",
  "org.hibernate" % "hibernate-entitymanager" % "5.2.10.Final",
  "com.h2database" % "h2" % "1.4.192",
  "org.assertj" % "assertj-core" % "3.8.0" % "test"
)