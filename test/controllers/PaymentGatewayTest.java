package controllers;

import entity.Account;
import entity.Money;
import entity.Payment;
import entity.Transaction;
import model.ExceptionalResponse;
import model.PaymentRequest;
import org.assertj.core.groups.Tuple;
import org.junit.Before;
import org.junit.Test;
import play.api.Play;
import play.db.jpa.JPAApi;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;
import repository.AccountRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static entity.Transaction.Type.DEBIT;
import static org.assertj.core.api.Assertions.assertThat;
import static play.test.Helpers.*;

public class PaymentGatewayTest extends WithApplication {

    private JPAApi jpaApi;

    private static final Account FIRST_ACCOUNT = new Account(1L, new Money(BigDecimal.TEN, "USD"));

    private static final Account SECOND_ACCOUNT = new Account(2L, new Money(BigDecimal.ZERO, "USD"));

    @Before
    public void initialize() {
        jpaApi = Play.current().injector().instanceOf(JPAApi.class);
    }

    @Test
    public void should_send_bad_request_and_receive_exceptional_response() {
        PaymentRequest paymentRequest = new PaymentRequest()
                .setSender(FIRST_ACCOUNT.getNumber())
                .setReceiver(SECOND_ACCOUNT.getNumber());
        Http.RequestBuilder createRequest = new Http.RequestBuilder()
                .method(POST)
                .uri("/payment")
                .bodyJson(Json.toJson(paymentRequest));

        Result createResult = route(app, createRequest);

        checkResponseStatus(createResult, BAD_REQUEST);

        assertThat(Json.fromJson(Json.parse(contentAsString(createResult)), ExceptionalResponse.class))
                .isNotNull()
                .extracting("errorMessage")
                .isNotEmpty()
                .containsOnly("Request is not correct");

    }

    @Test
    public void should_make_payment() throws InterruptedException {
        jpaApi.withTransaction(() -> {
            AccountRepository accountRepository = Play.current().injector().instanceOf(AccountRepository.class);
            accountRepository.save(FIRST_ACCOUNT);
            accountRepository.save(SECOND_ACCOUNT);
        });

        PaymentRequest paymentRequest = new PaymentRequest().setMoney(FIRST_ACCOUNT.getMoney())
                .setSender(FIRST_ACCOUNT.getNumber())
                .setReceiver(SECOND_ACCOUNT.getNumber());

        Payment payment = createPayment(paymentRequest);

        //Wait until payment will be processed
        //TODO: to implement waiting in cycle until payment will be processed
        Thread.sleep(1000);
        checkProcessingResult(payment);
        checkCountOfPaymentsAndCompareWithCreatedOne(payment);
    }

    private void checkCountOfPaymentsAndCompareWithCreatedOne(Payment payment) {
        Http.RequestBuilder loadAllRequest = new Http.RequestBuilder()
                .method(GET)
                .uri(String.format("/payment"));

        Result loadAllResult = route(app, loadAllRequest);

        checkResponseStatus(loadAllResult, OK);

        List<Payment> paymentList = new ArrayList<>();
        Json.parse(contentAsString(loadAllResult))
                .forEach(jsonNode -> paymentList.add(Json.fromJson(jsonNode, Payment.class)));

        assertThat(paymentList)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .containsOnly(payment);
    }

    private void checkProcessingResult(Payment createdPayment) {
        Http.RequestBuilder checkRequest = new Http.RequestBuilder()
                .method(GET)
                .uri(String.format("/payment/%s", createdPayment.getIdentifier()));
        Result processedResult = route(app, checkRequest);

        checkResponseStatus(processedResult, OK);

        Payment processedPayment = Json.fromJson(Json.parse(contentAsString(processedResult)), Payment.class);

        assertThat(processedPayment).isNotNull()
                .isEqualTo(createdPayment)
                .extracting("status")
                .isNotEmpty()
                .containsOnly(Payment.Status.COMPLETED);

        assertThat(processedPayment.getTransactions())
                .isNotEmpty()
                .hasSize(2)
                .extracting("type", "account", "account.money")
                .hasSize(2)
                .containsOnly(
                        Tuple.tuple(DEBIT, FIRST_ACCOUNT, SECOND_ACCOUNT.getMoney()),
                        Tuple.tuple(Transaction.Type.CREDIT, SECOND_ACCOUNT, FIRST_ACCOUNT.getMoney())
                );
    }

    private Payment createPayment(PaymentRequest paymentRequest) {
        Http.RequestBuilder createRequest = new Http.RequestBuilder()
                .method(POST)
                .uri("/payment")
                .bodyJson(Json.toJson(paymentRequest));

        Result makeResult = route(app, createRequest);

        checkResponseStatus(makeResult, OK);

        Payment payment = Json.fromJson(Json.parse(contentAsString(makeResult)), Payment.class);

        assertThat(payment)
                .isNotNull()
                .hasFieldOrPropertyWithValue("sender.number", paymentRequest.getSender())
                .hasFieldOrPropertyWithValue("receiver.number", paymentRequest.getReceiver())
                .hasFieldOrPropertyWithValue("money", paymentRequest.getMoney());

        return payment;
    }

    private void checkResponseStatus(Result result, int responseCode) {
        assertThat(result).isNotNull()
                .extracting("header.status")
                .hasSize(1)
                .containsOnly(responseCode);
    }

}
