package service;

import entity.Account;
import entity.Payment;
import entity.Transaction;
import play.Logger;
import play.db.jpa.JPAApi;
import repository.AccountRepository;
import repository.PaymentRepository;
import repository.TransactionRepository;
import strategy.ProcessingStrategy;
import strategy.ProcessingStrategyResolver;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Singleton
public class ProcessingService {

    //TODO: move count of threads in property file
    private ExecutorService executorService = Executors.newFixedThreadPool(4);

    @Inject
    private JPAApi jpaApi;

    @Inject
    private PaymentRepository paymentRepository;

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    private ProcessingStrategyResolver processingStrategyResolver;

    public void process(Payment payment) {
        executorService.submit(() -> {
            try {
                jpaApi.withTransaction(() -> {
                    Payment storedPayment = paymentRepository.findByIdentifier(payment.getIdentifier());
                    ProcessingStrategy processingStrategy = processingStrategyResolver.resolve(storedPayment);
                    processingStrategy.process(storedPayment);
                });

            } catch (Exception e) {
                Logger.error(e.getMessage(), e);
                jpaApi.withTransaction(() -> {
                    unblockMoney(payment);
                    failedPayment(payment, e);
                });
            }
        });
    }

    private void unblockMoney(Payment payment) {
        Account sender = payment.getSender();
        sender.unblockMoney(payment);
        accountRepository.update(sender);
    }

    private void failedPayment(Payment payment, Exception e) {
        Transaction transaction = new Transaction(payment, payment.getSender(), payment.getMoney(), Transaction.Type.DEBIT, Transaction.Status.FAILED, e.getMessage());
        transactionRepository.save(transaction);
        payment.setStatus(Payment.Status.FAILED);
        payment.addTransaction(transaction);
        paymentRepository.update(payment);
    }

}
