package service;

import entity.Account;
import entity.Payment;
import entity.Transaction;
import model.PaymentRequest;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import repository.AccountRepository;
import repository.PaymentRepository;
import repository.TransactionRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

import static entity.Transaction.Status.FAILED;
import static entity.Transaction.Type.DEBIT;

@Singleton
public class PaymentService {

    @Inject
    private JPAApi jpaApi;

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private PaymentRepository paymentRepository;

    @Inject
    private ProcessingService processingService;

    @Inject
    private TransactionRepository transactionRepository;

    @Transactional
    public Payment makePayment(PaymentRequest request){
        if (request.isNotCorrect()) throw new RuntimeException("Request is not correct");
        Account sender = accountRepository.findByNumber(request.getSender());
        Account receiver = accountRepository.findByNumber(request.getReceiver());
        Payment payment = new Payment(sender, receiver, request.getMoney());

        jpaApi.withTransaction(()-> {
            paymentRepository.save(payment);
            if (sender.moneyAvailable(payment.getMoney()))
                blockMoneyOnSenderAccount(payment);
            else {
                Transaction transaction = new Transaction(payment, sender, payment.getMoney(), DEBIT, FAILED, "Not enough money");
                transactionRepository.save(transaction);
                payment.addTransaction(transaction);
                payment.setStatus(Payment.Status.FAILED);
                paymentRepository.update(payment);
            }
        });
        if (!payment.isFailed()) processingService.process(payment);
        return payment;
    }

    private void blockMoneyOnSenderAccount(Payment payment) {
        Account sender = payment.getSender();
        sender.blockMoney(payment);
        accountRepository.update(sender);
    }

    public Payment load(String identifier) {
        return paymentRepository.findByIdentifier(identifier);
    }

    public List<Payment> loadAll() {
        return paymentRepository.findAll();
    }
}
