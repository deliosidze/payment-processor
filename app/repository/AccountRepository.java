package repository;

import entity.Account;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@Transactional
public class AccountRepository extends CrudRepository<Account, Long> {

    public @Inject
    AccountRepository(JPAApi jpaApi) {
        super(Account.class, jpaApi);
    }

    public Account findByNumber(Long number) {
        return getJpaApi().withTransaction(em ->
                em.createQuery("from Account a where a.number=:number", Account.class)
                        .setParameter("number", number)
                        .getSingleResult());
    }
}
