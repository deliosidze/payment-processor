package repository;

import entity.Payment;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PaymentRepository extends CrudRepository<Payment, Long>{

    public @Inject PaymentRepository(JPAApi jpaApi) {
        super(Payment.class, jpaApi);
    }

    public Payment findByIdentifier(String identifier) {
        return getJpaApi().withTransaction(em -> em.createQuery("from Payment p where p.identifier=:identifier", Payment.class)
                .setParameter("identifier", identifier).getSingleResult());
    }
}
