package repository;

import play.db.jpa.JPAApi;

import java.util.List;

abstract class CrudRepository<T, I> {

    private JPAApi jpaApi;

    private Class<T> aClass;

    public CrudRepository(Class<T> aClass, JPAApi jpaApi) {
        this.aClass = aClass;
        this.jpaApi = jpaApi;
    }

    public T find(I id){
        return jpaApi.em().find(aClass, id);
    }

    public T save(T entity){
        jpaApi.em().persist(entity);
        jpaApi.em().flush();
        return entity;
    }

    public T update(T entity){
        jpaApi.em().merge(entity);
        jpaApi.em().flush();
        return entity;
    }

    public T reference(I id){
        return jpaApi.em().getReference(aClass, id);
    }

    public List<T> findAll(){
        return jpaApi.em().createQuery(String.format("from %s", aClass.getCanonicalName())).getResultList();
    }

    protected JPAApi getJpaApi(){
        return jpaApi;
    }

}
