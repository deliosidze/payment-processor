package repository;

import entity.Transaction;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TransactionRepository extends CrudRepository<Transaction, Long> {

    public @Inject TransactionRepository(JPAApi jpaApi) {
        super(Transaction.class, jpaApi);
    }

}
