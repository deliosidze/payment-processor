package controllers.handler;

import model.ExceptionalResponse;
import play.http.HttpErrorHandler;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


//TODO: To fix it.
//TODO: It doesn't work and I don't know why
@Singleton
public class ExceptionHandler implements HttpErrorHandler {

    @Override
    public CompletionStage<Result> onClientError(Http.RequestHeader request, int statusCode, String message) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CompletionStage<Result> onServerError(Http.RequestHeader request, Throwable exception) {
        ExceptionalResponse exceptionalResponse = new ExceptionalResponse(exception.getClass().getName(), exception.getMessage());
        return CompletableFuture.completedFuture(Results.badRequest(Json.toJson(exceptionalResponse)));
    }


}
