package controllers;

import model.ExceptionalResponse;
import model.PaymentRequest;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.PaymentService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PaymentGateway extends Controller {

    @Inject
    private PaymentService paymentService;

    public Result make() {
        PaymentRequest paymentRequest = Json.fromJson(request().body().asJson(), PaymentRequest.class);
        try {
            return ok(Json.toJson(paymentService.makePayment(paymentRequest)));
        } catch (Exception ex) {
            Logger.error(ex.getMessage(), ex);
            ExceptionalResponse exceptionalResponse = new ExceptionalResponse(ex.getClass().getName(), ex.getMessage());
            return badRequest(Json.toJson(exceptionalResponse));
        }
    }

    @Transactional
    public Result loadOne(String identifier) {
        try {
            return ok(Json.toJson(paymentService.load(identifier)));
        } catch (Exception ex) {
            ExceptionalResponse exceptionalResponse = new ExceptionalResponse(ex.getClass().getName(), ex.getMessage());
            return badRequest(Json.toJson(exceptionalResponse));
        }

    }

    @Transactional
    public Result loadAll() {
        try {
            return ok(Json.toJson(paymentService.loadAll()));
        } catch (Exception ex) {
            ExceptionalResponse exceptionalResponse = new ExceptionalResponse(ex.getClass().getName(), ex.getMessage());
            return badRequest(Json.toJson(exceptionalResponse));
        }
    }

}
