package strategy;

import entity.Payment;

public interface ProcessingStrategy {

    void process(Payment payment);

}
