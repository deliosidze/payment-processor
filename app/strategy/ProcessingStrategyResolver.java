package strategy;

import entity.Payment;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ProcessingStrategyResolver {

    @Inject
    private SingleCurrencyProcessingStrategy singleCurrencyProcessingStrategy;

    @Inject
    private MultiCurrencyProcessingStrategy multiCurrencyProcessingStrategy;


    public ProcessingStrategy resolve(Payment payment) {
        if (payment.isMultiCurrency()) return multiCurrencyProcessingStrategy;
        return singleCurrencyProcessingStrategy;
    }

}
