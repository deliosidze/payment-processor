package strategy;

import entity.Payment;
import entity.Transaction;
import repository.PaymentRepository;

import javax.inject.Inject;

import static entity.Transaction.Status.FAILED;
import static entity.Transaction.Type.BLOCK;

public class MultiCurrencyProcessingStrategy implements ProcessingStrategy {

    @Inject
    private PaymentRepository paymentRepository;

    @Override
    public void process(Payment payment) {
        Transaction transaction = new Transaction(payment, payment.getSender(), payment.getMoney(), BLOCK, FAILED, "System does not support multi currency transactions");
        payment.addTransaction(transaction);
        payment.setStatus(Payment.Status.FAILED);
        paymentRepository.save(payment);
    }

}
