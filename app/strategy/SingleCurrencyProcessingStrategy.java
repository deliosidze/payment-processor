package strategy;

import entity.Account;
import entity.Payment;
import entity.Transaction;
import play.db.jpa.Transactional;
import repository.AccountRepository;
import repository.PaymentRepository;
import repository.TransactionRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import static entity.Transaction.Status.FAILED;
import static entity.Transaction.Status.SUCCESS;
import static entity.Transaction.Type.CREDIT;
import static entity.Transaction.Type.DEBIT;

@Singleton
public class SingleCurrencyProcessingStrategy implements ProcessingStrategy {

    @Inject
    private PaymentRepository paymentRepository;

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private TransactionRepository transactionRepository;

    @Override
    @Transactional
    public void process(Payment payment) {
        debitSender(payment);
        creditReceiver(payment);
        paymentRepository.update(payment);
    }

    private Payment creditReceiver(Payment payment) {
        Account receiver = payment.getReceiver();
        receiver.addFounds(payment.getMoney());
        accountRepository.update(receiver);
        Transaction credit = new Transaction(payment, receiver, payment.getMoney(), CREDIT, SUCCESS);
        transactionRepository.save(credit);
        payment.addTransaction(credit);
        payment.setStatus(Payment.Status.COMPLETED);
        return payment;
    }

    private Payment debitSender(Payment payment) {
        Account sender = payment.getSender();
        sender.chargeMoney(payment);
        accountRepository.update(sender);
        Transaction debit = new Transaction(payment, sender, payment.getMoney(), DEBIT, SUCCESS);
        transactionRepository.save(debit);
        payment.addTransaction(debit);
        payment.setStatus(Payment.Status.IN_PROGRESS);
        return payment;
    }

}
