package model;

import entity.Money;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class PaymentRequest {

    private Money money;

    private Long sender;

    private Long receiver;

    public Money getMoney() {
        return money;
    }

    public PaymentRequest setMoney(Money money) {
        this.money = money;
        return this;
    }

    public Long getSender() {
        return sender;
    }

    public PaymentRequest setSender(Long sender) {
        this.sender = sender;
        return this;
    }

    public Long getReceiver() {
        return receiver;
    }

    public PaymentRequest setReceiver(Long receiver) {
        this.receiver = receiver;
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PaymentRequest that = (PaymentRequest) o;

        return new EqualsBuilder()
                .append(sender, that.sender)
                .append(receiver, that.receiver)
                .append(money, that.money)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(money)
                .append(sender)
                .append(receiver)
                .toHashCode();
    }

    public boolean isNotCorrect() {
        return (sender==null || sender<1)
                || (receiver==null || receiver<1)
                || (money==null || money.getAmount().doubleValue()<0.01);
    }
}
