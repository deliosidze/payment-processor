package model;

public class ExceptionalResponse {

    private String errorType;

    private String errorMessage;

    public ExceptionalResponse(String errorType, String errorMessage) {
        this.errorType = errorType;
        this.errorMessage = errorMessage;
    }

    public ExceptionalResponse() {
    }

    public String getErrorType() {
        return errorType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
