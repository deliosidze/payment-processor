package entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;
import java.util.Currency;

@Embeddable
public class Money {

    @Column(nullable = false)
    private BigDecimal amount;

    @Column(nullable = false)
    private Currency currency;

    public Money(BigDecimal amount, String currency) {
        this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        this.currency = Currency.getInstance(currency);
    }

    public Money() {
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Money setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Money setCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        return new EqualsBuilder()
                .append(amount.doubleValue(), money.amount.doubleValue())
                .append(currency, money.currency)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(amount.doubleValue())
                .append(currency)
                .toHashCode();
    }
}
