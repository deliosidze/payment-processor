package entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static entity.Payment.Status.*;

@Entity
@Access(AccessType.FIELD)
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String identifier;

    @Column(nullable = false)
    private Timestamp date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender_id", referencedColumnName = "id")
    private Account sender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "receiver_id", referencedColumnName = "id")
    private Account receiver;

    @Embedded
    private Money money;

    @Column(nullable = false)
    private Status status;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "payment")
    private Set<Transaction> transactions;

    public Payment(Account sender, Account receiver, Money money) {
        this.date = Timestamp.from(Instant.now());
        this.identifier = UUID.randomUUID().toString();
        this.sender = sender;
        this.receiver = receiver;
        this.money = money;
        this.status = IN_PROGRESS;
    }

    public Payment() {
    }

    public Long getId() {
        return id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Timestamp getDate() {
        return date;
    }

    public Account getSender() {
        return sender;
    }

    public Account getReceiver() {
        return receiver;
    }

    public Money getMoney() {
        return money;
    }

    public Status getStatus() {
        return status;
    }

    public Payment setStatus(Status status) {
        this.status = status;
        return this;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    @JsonIgnore
    public boolean isMultiCurrency() {
        return sender.notInCurrency(money.getCurrency()) || receiver.notInCurrency(money.getCurrency());
    }

    public void addTransaction(Transaction transaction) {
        if (transactions==null) transactions=new HashSet<>();
        transactions.add(transaction);
    }

    @JsonIgnore
    public boolean isFailed() {
        return status==FAILED;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Payment payment = (Payment) o;

        return new EqualsBuilder()
                .append(identifier, payment.identifier)
                .append(date, payment.date)
                .append(money, payment.money)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(identifier)
                .append(date)
                .append(money)
                .toHashCode();
    }


    public enum Status {
        IN_PROGRESS, FAILED, COMPLETED
    }

}
