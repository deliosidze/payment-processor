package entity;

import javax.persistence.*;
import java.util.UUID;

@Embeddable
public class BlockedMoney {

    @Column(unique = true)
    private String identifier;

    @Embedded
    private Money money;

    @OneToOne
    @JoinColumn(name = "payment_id", referencedColumnName = "id")
    private Payment payment;

    public BlockedMoney() {
    }

    public BlockedMoney(Payment payment) {
        this.identifier = UUID.randomUUID().toString();
        this.money = payment.getMoney();
        this.payment = payment;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Money getMoney() {
        return money;
    }

    public Payment getPayment() {
        return payment;
    }

}
