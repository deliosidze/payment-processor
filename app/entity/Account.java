package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.Currency;
import java.util.HashSet;
import java.util.Set;

@Entity
@Access(AccessType.FIELD)
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private Long number;

    @Embedded
    private Money money;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "account_block",
            joinColumns = @JoinColumn(name = "account_id"))
    private Set<BlockedMoney> blockedMoneys;


    @OneToMany(mappedBy = "account")
    private Set<Transaction> transactions;

    private Status status;

    public Account(Long number, Money money) {
        this.number = number;
        this.money = money;
        this.status = Status.OPEN;
    }

    public Account() {
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public Long getNumber() {
        return number;
    }

    public Money getMoney() {
        return money;
    }

    @JsonIgnore
    public Set<BlockedMoney> getBlockedMoneys() {
        return blockedMoneys;
    }

    public Status getStatus() {
        return status;
    }

    @JsonIgnore
    public Set<Transaction> getTransactions() {
        return transactions;
    }

    @JsonIgnore
    public boolean notInCurrency(Currency currency) {
        return !money.getCurrency().equals(currency);
    }

    @JsonIgnore
    public boolean moneyAvailable(Money money) {
        return this.money.getAmount().subtract(money.getAmount()).doubleValue()>=0;
    }

    public void chargeMoney(Payment payment) {
        BlockedMoney blockedMoney = blockedMoneys.stream()
                .filter(bm -> bm.getPayment().equals(payment)).findAny().get();
        blockedMoneys.remove(blockedMoney);
    }

    public void addFounds(Money money) {
        this.money.setAmount(this.money.getAmount().add(money.getAmount()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return new EqualsBuilder()
                .append(number, account.number)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(number)
                .toHashCode();
    }

    public void blockMoney(Payment payment) {
        this.money.setAmount(this.money.getAmount().subtract(money.getAmount()));
        if (blockedMoneys==null) blockedMoneys = new HashSet<>();
        blockedMoneys.add(new BlockedMoney(payment));
    }

    public void unblockMoney(Payment payment) {
        BlockedMoney blockedMoney = blockedMoneys.stream()
                .filter(bm -> bm.getPayment().equals(payment)).findAny().get();
        addFounds(blockedMoney.getMoney());
        blockedMoneys.remove(blockedMoney);
    }

    public enum Status {
        OPEN, CLOSED
    }

}
