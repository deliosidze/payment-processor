package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String identifier;

    @Column(nullable = false)
    private Timestamp date;

    @ManyToOne
    @JoinColumn(name = "payment_id", referencedColumnName = "id")
    private Payment payment;

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;

    @Embedded
    private Money money;

    @Column(nullable = false)
    private Type type;

    @Column(nullable = false)
    private Status status;

    private String processingIssue;

    public Transaction(Payment payment, Account account, Money money, Type type, Status status, String processingIssue) {
        this.payment = payment;
        this.identifier = UUID.randomUUID().toString();
        this.date = Timestamp.from(Instant.now());
        this.account = account;
        this.money = money;
        this.type = type;
        this.status = status;
        this.processingIssue = processingIssue;
    }

    public Transaction(Payment payment, Account account, Money money, Type type, Status status) {
        this.payment = payment;
        this.identifier = UUID.randomUUID().toString();
        this.date = Timestamp.from(Instant.now());
        this.account = account;
        this.money = money;
        this.type = type;
        this.status = status;
    }

    public Transaction() {
    }

    public String getIdentifier() {
        return identifier;
    }

    public Account getAccount() {
        return account;
    }

    public Type getType() {
        return type;
    }

    public Transaction setType(Type type) {
        this.type = type;
        return this;
    }

    public String getProcessingIssue() {
        return processingIssue;
    }

    public Status getStatus() {
        return status;
    }

    public Transaction setStatus(Status status) {
        this.status = status;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Timestamp getDate() {
        return date;
    }

    @JsonIgnore
    public Payment getPayment() {
        return payment;
    }

    public Money getMoney() {
        return money;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        return new EqualsBuilder()
                .append(identifier, that.identifier)
                .append(date, that.date)
                .append(account, that.account)
                .append(money, that.money)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(identifier)
                .append(date)
                .append(account)
                .toHashCode();
    }

    public enum Type {
        BLOCK, DEBIT, CREDIT
    }

    public enum Status {
        SUCCESS, FAILED
    }

}
